let allCountries = require('./data/allCountries.json')
let argv = require('minimist')(process.argv.slice(2));
let request = require('request');
let webshot = require('webshot');
let async = require('async');
let baseUrl = argv.baseUrl ? argv.baseUrl : "https://play.google.com/store/apps/collection/promotion_3000791_new_releases_games\?gl\=";
let fs = require('fs');
let jsdom = require('jsdom');
let { JSDOM } = jsdom;
let finished = true;

let found = {
    found: [],
    notFound: []
}
let getPicture = function getPicture(country, countryCode, callback) {
    finished = false;
    let url = baseUrl+countryCode;
    request(url, function (error, response, body) {
        if(error) {
            console.error(error);
            return callback(null, error);
        }
        if (body.indexOf(argv.game) != -1) {
            console.log(argv.game +" found in " + country);
            let position = getPositionOfGame(argv.game, body);
            found.found.push(country + ", position: " + position);
            if (argv.saveImage) {
                takeAScreenshot(callback);
            }
            else {
                return callback();
            }
        }
        else {
            console.log(argv.game +" NOT found in " + country + " :-(");
            found.notFound.push(country);
            return callback();
        }
    });
};

let main = function () {
    let functions = [];
    for (let i in allCountries) {
        functions.push(
            function (callback) {
                getPicture(i, allCountries[i], callback);
            }
        );  
    }
    
    
    async.parallel(functions, function (error) {
        if (error) {
            console.log(error);
        }
        let gameName = argv.game.split(" ").join("_");
        console.log("Done, check files found_"+gameName+".txt and notFound_"+gameName+".txt.");
        fs.writeFileSync("found_"+gameName+".txt", found.found.join("\n"));
        fs.writeFileSync("notFound_"+gameName+".txt", found.notFound.join("\n"));
    });
    
}

let getPositionOfGame = function (game, body) {
    let dom = new JSDOM(body);
    let data = dom.window.document.getElementsByClassName("card");
    let position = -1;
    for (i in data) {
        if (typeof(data[i]) == "object") {
            if (game.toLowerCase() == data[i].querySelector("div > div a[class='title']").text.trim().toLowerCase()) {
                position = parseInt(i, 10) + 1;
                break;
            }
        }
    }
    return position;
}

let takeAScreenshot = function (callback) {
    let webshotConfig = {
        windowSize: {
            width: 1920,
            height: 1080
        },
        shotSize: {
            width: 1920,
            height: "all"
        }
    };
    webshot(url, "images/"+country+"_" + argv.game + ".png", webshotConfig, function (err) {
        if (err){
            console.error(err);
        }
        console.log("Screenshot saved for " + country +"!");
        finished = false;
        return callback(null);
    });
}

//run main loop

main();